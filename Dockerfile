FROM fedora

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG TZ
ARG http_proxy
ARG https_proxy

RUN set -x \
	&& dnf install -y \
		httpd \
		ruby \
		procps-ng \
		net-tools \
		diffutils \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

# git clone http://gitlab.com/goldenfishtiger/rubycgi_root しておくこと
COPY rubycgi_root /var/www/cgi-bin/
RUN cp /var/www/cgi-bin/dot.htaccess /var/www/cgi-bin/.htaccess

RUN set -x \
	&& mv /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.org \
	&& cat /etc/httpd/conf/httpd.conf.org \
		| sed '/^<Directory "\/var\/www\/cgi-bin">/,/^</s/AllowOverride None/AllowOverride All/' \
		| sed '/^<Directory "\/var\/www\/cgi-bin">/,/^</s/Options None/Options All/' \
		> /etc/httpd/conf/httpd.conf \
	&& diff -C 2 /etc/httpd/conf/httpd.conf.org /etc/httpd/conf/httpd.conf \
	|| echo '/etc/httpd/conf/httpd.conf changed.'

RUN systemctl enable httpd

EXPOSE 80

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN/,/^##__END/p\' | sed \'s/^#//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN__startup.sh__
#
#	echo "TZ='$TZ'"
#	[ -n "$TZ" ] && ln -sfv /usr/share/zoneinfo/$TZ /etc/localtime
#	chown -R -v apache:apache /var/www/cgi-bin/data
#	echo 'startup.sh done.'
#
##__END__startup.sh__

